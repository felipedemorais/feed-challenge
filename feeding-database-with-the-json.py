# -*- coding: utf-8 -*-
import json

"""
It was created to feed the database with the data.json. To run it use:
$./manage.py shell
$execfile(feeding-database-with-the-json.py)
"""


from feed.models import Author, Image, Video, Post

data_json = open('feed/fixtures/data.json')
data_as_a_python_dict = json.load(data_json)
counter = 0

for post in data_as_a_python_dict['postList']:
    print 'Salvando o número %s' % counter

    my_image = None
    my_video = None

    if post['author']:
        my_author = Author()
        my_author.name = post['author']['name']
        if post['author']['role'] == 'Famoso':
            my_author.role = True
        else:
            my_author.role = False
        my_author.avatar = post['author']['avatar']
        my_author.save()

    if post['images']:
        for image in post['images']:
            my_image = Image()
            my_image.credits = image['credits']
            my_image.croppedUrl = image['croppedUrl']
            my_image.imageFeedUrl = image['imageFeedUrl']
            my_image.imageLightUrl = image['imageLightUrl']
            my_image.subject = image['subject']
            my_image.save()

    if post['videos']:
        for video in post['videos']:
            my_video = Video()
            my_video.id = video['id']
            my_video.thumb = video['thumb']
            my_video.save()

    my_post = Post()
    my_post.body = post['body']
    my_post.status = 'P'
    my_post.author = my_author

    if my_image:
        my_post.image = my_image

    if my_video:
        my_post.video = my_video

    my_post.save()
    print 'Terminei de salvar o número %s' % counter
    counter += 1
