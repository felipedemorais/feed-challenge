# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('role', models.BooleanField(default=False)),
                ('avatar', models.URLField(null=True, blank=True)),
                ('name', models.CharField(default=b'Desconhecido', max_length=40)),
            ],
            options={
                'verbose_name': 'Author',
                'verbose_name_plural': 'Authors',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField()),
                ('croppedUrl', models.URLField(max_length=300, null=True, blank=True)),
                ('credits', models.CharField(max_length=50, null=True, blank=True)),
                ('imageLightUrl', models.URLField(max_length=300, null=True, blank=True)),
                ('imageFeedUrl', models.URLField(max_length=300, null=True, blank=True)),
                ('subject', models.CharField(max_length=50, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Image',
                'verbose_name_plural': 'Images',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(max_length=30, serialize=False, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('body', models.CharField(max_length=600)),
                ('status', models.CharField(default=b'P', max_length=11, choices=[('U', 'Unpublished'), ('P', 'Published'), ('S', 'Scheduled'), ('D', 'Draft')])),
                ('author', models.ForeignKey(blank=True, to='feed.Author', null=True)),
                ('image', models.ForeignKey(default=None, blank=True, to='feed.Image', null=True)),
            ],
            options={
                'verbose_name': 'Post',
                'verbose_name_plural': 'Posts',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(max_length=30, serialize=False, primary_key=True)),
                ('thumb', models.URLField()),
            ],
            options={
                'verbose_name': 'Video',
                'verbose_name_plural': 'Videos',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='post',
            name='video',
            field=models.ForeignKey(default=None, blank=True, to='feed.Video', null=True),
            preserve_default=True,
        ),
    ]
