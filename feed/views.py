# -*- coding: utf-8 -*-

from django.views.generic.base import TemplateView
from django.http import HttpResponsePermanentRedirect
from django.utils.html import escape
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from feed.models import Post


class FeedView(TemplateView):

    template_name = "index.html"

    def post(self, request, *args, **kwargs):
        post = Post()
        post.body = escape(request.POST["body"])
        post.status = "P"

        try:
            post.full_clean()
        except ValidationError as e:
            if u'This field cannot be blank.' in e.messages:
                QUERY_STRING = '?error=true'
                return HttpResponsePermanentRedirect(reverse('index') + QUERY_STRING)
        else:
            post.save()
        return HttpResponsePermanentRedirect(reverse('index'))

    def get_context_data(self, **kwargs):
        if dict.get(self.request.GET, 'error', ''):
            error_message = 'Não é possível salvar um comentário vazio. Por favor preencha o comentário.'
            context = {'error_message': error_message}
        else:
            context = {}

        posts = Post.objects.filter(status='P').extra(order_by=['-created_at'])
        context['postList'] = posts

        return context
