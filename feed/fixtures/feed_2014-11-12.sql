﻿# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.16)
# Database: feed
# Generation Time: 2014-11-13 01:27:08 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table auth_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_group`;

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table auth_group_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_group_permissions`;

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_0e939a4f` (`group_id`),
  KEY `auth_group_permissions_8373b171` (`permission_id`),
  CONSTRAINT `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permission_group_id_689710a9a73b7457_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table auth_permission
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_permission`;

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_417f1b1c` (`content_type_id`),
  CONSTRAINT `auth__content_type_id_508cf46651277a81_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`)
VALUES
	(1,'Can add permission',1,'add_permission'),
	(2,'Can change permission',1,'change_permission'),
	(3,'Can delete permission',1,'delete_permission'),
	(4,'Can add group',2,'add_group'),
	(5,'Can change group',2,'change_group'),
	(6,'Can delete group',2,'delete_group'),
	(7,'Can add user',3,'add_user'),
	(8,'Can change user',3,'change_user'),
	(9,'Can delete user',3,'delete_user'),
	(10,'Can add content type',4,'add_contenttype'),
	(11,'Can change content type',4,'change_contenttype'),
	(12,'Can delete content type',4,'delete_contenttype'),
	(13,'Can add session',5,'add_session'),
	(14,'Can change session',5,'change_session'),
	(15,'Can delete session',5,'delete_session'),
	(16,'Can add site',6,'add_site'),
	(17,'Can change site',6,'change_site'),
	(18,'Can delete site',6,'delete_site'),
	(19,'Can add Author',7,'add_author'),
	(20,'Can change Author',7,'change_author'),
	(21,'Can delete Author',7,'delete_author'),
	(22,'Can add Video',8,'add_video'),
	(23,'Can change Video',8,'change_video'),
	(24,'Can delete Video',8,'delete_video'),
	(25,'Can add Image',9,'add_image'),
	(26,'Can change Image',9,'change_image'),
	(27,'Can delete Image',9,'delete_image'),
	(28,'Can add Post',10,'add_post'),
	(29,'Can change Post',10,'change_post'),
	(30,'Can delete Post',10,'delete_post');

/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_user`;

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table auth_user_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_user_groups`;

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_e8701ad4` (`user_id`),
  KEY `auth_user_groups_0e939a4f` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table auth_user_user_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_user_user_permissions`;

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_e8701ad4` (`user_id`),
  KEY `auth_user_user_permissions_8373b171` (`permission_id`),
  CONSTRAINT `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissi_user_id_7f0938558328534a_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table django_content_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `django_content_type`;

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;

INSERT INTO `django_content_type` (`id`, `name`, `app_label`, `model`)
VALUES
	(1,'permission','auth','permission'),
	(2,'group','auth','group'),
	(3,'user','auth','user'),
	(4,'content type','contenttypes','contenttype'),
	(5,'session','sessions','session'),
	(6,'site','sites','site'),
	(7,'Author','feed','author'),
	(8,'Video','feed','video'),
	(9,'Image','feed','image'),
	(10,'Post','feed','post');

/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `django_migrations`;

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`)
VALUES
	(1,'contenttypes','0001_initial','2014-11-13 01:18:18'),
	(2,'auth','0001_initial','2014-11-13 01:18:24'),
	(3,'feed','0001_initial','2014-11-13 01:18:27'),
	(4,'sessions','0001_initial','2014-11-13 01:18:28'),
	(5,'sites','0001_initial','2014-11-13 01:18:28');

/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_session
# ------------------------------------------------------------

DROP TABLE IF EXISTS `django_session`;

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table django_site
# ------------------------------------------------------------

DROP TABLE IF EXISTS `django_site`;

CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;

INSERT INTO `django_site` (`id`, `domain`, `name`)
VALUES
	(1,'example.com','example.com');

/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table feed_author
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feed_author`;

CREATE TABLE `feed_author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` tinyint(1) NOT NULL,
  `avatar` varchar(200) DEFAULT NULL,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

LOCK TABLES `feed_author` WRITE;
/*!40000 ALTER TABLE `feed_author` DISABLE KEYS */;

INSERT INTO `feed_author` (`id`, `role`, `avatar`, `name`)
VALUES
	(1,1,'https://graph.facebook.com/100007906810529/picture','Produção Bem Estar'),
	(2,1,'https://graph.facebook.com/100008099917219/picture','Dra Márcia Purceli'),
	(3,1,'https://graph.facebook.com/100008099917219/picture','Dra Márcia Purceli'),
	(4,1,'https://graph.facebook.com/100008099917219/picture','Dra Márcia Purceli'),
	(5,0,'https://graph.facebook.com/100001057835042/picture','Juliana Fernandes'),
	(6,0,'','Tatiane Dias'),
	(7,1,'https://graph.facebook.com/100008099917219/picture','Dra Márcia Purceli'),
	(8,0,'','Alexandre Paixao Costa'),
	(9,0,'','Thales Nascimento Souza'),
	(10,0,'','Valeriana Cristina Algusto'),
	(11,1,'https://graph.facebook.com/100007906810529/picture','Produção Bem Estar'),
	(12,1,'https://graph.facebook.com/100008099917219/picture','Dra Márcia Purceli'),
	(13,1,'https://graph.facebook.com/100008099917219/picture','Dra Márcia Purceli'),
	(14,1,'https://graph.facebook.com/100008099917219/picture','Dra Márcia Purceli'),
	(15,0,'https://graph.facebook.com/100001057835042/picture','Juliana Fernandes'),
	(16,0,'','Tatiane Dias'),
	(17,1,'https://graph.facebook.com/100008099917219/picture','Dra Márcia Purceli'),
	(18,0,'','Alexandre Paixao Costa'),
	(19,0,'','Thales Nascimento Souza'),
	(20,0,'','Valeriana Cristina Algusto'),
	(21,1,'https://graph.facebook.com/100007906810529/picture','Produção Bem Estar'),
	(22,1,'https://graph.facebook.com/100008099917219/picture','Dra Márcia Purceli'),
	(23,1,'https://graph.facebook.com/100008099917219/picture','Dra Márcia Purceli'),
	(24,1,'https://graph.facebook.com/100008099917219/picture','Dra Márcia Purceli'),
	(25,0,'https://graph.facebook.com/100001057835042/picture','Juliana Fernandes'),
	(26,0,'','Tatiane Dias'),
	(27,1,'https://graph.facebook.com/100008099917219/picture','Dra Márcia Purceli'),
	(28,0,'','Alexandre Paixao Costa'),
	(29,0,'','Thales Nascimento Souza'),
	(30,0,'','Valeriana Cristina Algusto');

/*!40000 ALTER TABLE `feed_author` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table feed_image
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feed_image`;

CREATE TABLE `feed_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) NOT NULL,
  `croppedUrl` varchar(300) DEFAULT NULL,
  `credits` varchar(50) DEFAULT NULL,
  `imageLightUrl` varchar(300) DEFAULT NULL,
  `imageFeedUrl` varchar(300) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

LOCK TABLES `feed_image` WRITE;
/*!40000 ALTER TABLE `feed_image` DISABLE KEYS */;

INSERT INTO `feed_image` (`id`, `url`, `croppedUrl`, `credits`, `imageLightUrl`, `imageFeedUrl`, `subject`)
VALUES
	(1,'','http://s2.glbimg.com/k0QZIZ3DXrNP8Y7sN4EZhcLfMl8=/640x0/s2.glbimg.com/v5-p6Xn_iWYSljXn1iSamjRxUmI=/s.glbimg.com/in/fo/2014/10/24/10/40/13/ac104bf1-d7a2-49e6-9c07-83a6467a5684.jpg','','http://s2.glbimg.com/d3MwekLXgUgXrJXOjunYUUt29YQ=/fit-in/1080x1080/s2.glbimg.com/k0QZIZ3DXrNP8Y7sN4EZhcLfMl8=/640x0/s2.glbimg.com/v5-p6Xn_iWYSljXn1iSamjRxUmI=/s.glbimg.com/in/fo/2014/10/24/10/40/13/ac104bf1-d7a2-49e6-9c07-83a6467a5684.jpg','http://s2.glbimg.com/L5vY1fdEoIsNIKzCZmBqEh6BKy8=/fit-in/718x0/s2.glbimg.com/k0QZIZ3DXrNP8Y7sN4EZhcLfMl8=/640x0/s2.glbimg.com/v5-p6Xn_iWYSljXn1iSamjRxUmI=/s.glbimg.com/in/fo/2014/10/24/10/40/13/ac104bf1-d7a2-49e6-9c07-83a6467a5684.jpg',''),
	(2,'','http://s2.glbimg.com/Wa4xW4iqPrTK80lGV23QVAMfeUc=/640x0/s2.glbimg.com/Zo2DAx2-S5pjZfrzqWNlu8PGpRw=/s.glbimg.com/in/fo/2014/10/24/10/32/25/65e6bb11-dc11-40b5-9f55-76f16c23fac3.jpg','','http://s2.glbimg.com/5z6dr-30t1EdaV_AqiU7-ZgmdM8=/fit-in/1080x1080/s2.glbimg.com/Wa4xW4iqPrTK80lGV23QVAMfeUc=/640x0/s2.glbimg.com/Zo2DAx2-S5pjZfrzqWNlu8PGpRw=/s.glbimg.com/in/fo/2014/10/24/10/32/25/65e6bb11-dc11-40b5-9f55-76f16c23fac3.jpg','http://s2.glbimg.com/RRW-w7Yvp9qZ4B36jtO3mGAPd_A=/fit-in/718x0/s2.glbimg.com/Wa4xW4iqPrTK80lGV23QVAMfeUc=/640x0/s2.glbimg.com/Zo2DAx2-S5pjZfrzqWNlu8PGpRw=/s.glbimg.com/in/fo/2014/10/24/10/32/25/65e6bb11-dc11-40b5-9f55-76f16c23fac3.jpg',''),
	(3,'','http://s2.glbimg.com/f9QNUM3FBOSCHo_9O-nGMGo8mZ4=/640x0/s2.glbimg.com/UOv5UeDsFsdvNTQZjLR9Wrh717Y=/s.glbimg.com/in/fo/2014/10/24/10/26/40/cf35c3d8-5272-4298-ab88-9c845a9e945e.jpg','','http://s2.glbimg.com/HsqRIsiz5c54dYjBWxtx6G1Q8LU=/fit-in/1080x1080/s2.glbimg.com/f9QNUM3FBOSCHo_9O-nGMGo8mZ4=/640x0/s2.glbimg.com/UOv5UeDsFsdvNTQZjLR9Wrh717Y=/s.glbimg.com/in/fo/2014/10/24/10/26/40/cf35c3d8-5272-4298-ab88-9c845a9e945e.jpg','http://s2.glbimg.com/kbQC929DyCSn4fVz4_RwtMEZPbg=/fit-in/718x0/s2.glbimg.com/f9QNUM3FBOSCHo_9O-nGMGo8mZ4=/640x0/s2.glbimg.com/UOv5UeDsFsdvNTQZjLR9Wrh717Y=/s.glbimg.com/in/fo/2014/10/24/10/26/40/cf35c3d8-5272-4298-ab88-9c845a9e945e.jpg',''),
	(4,'','http://s2.glbimg.com/QpqZONR2SA8m7VRhzNmw8C9DVc0=/640x0/s2.glbimg.com/NHXKJvIJUxv8wUlkB_BkffPq65k=/s.glbimg.com/in/fo/2014/10/24/10/23/29/4cd49797-9340-401b-9308-569074318e70.jpg','','http://s2.glbimg.com/WPOiwz7jYuXgcoNSPfNeXXSd1Dk=/fit-in/1080x1080/s2.glbimg.com/QpqZONR2SA8m7VRhzNmw8C9DVc0=/640x0/s2.glbimg.com/NHXKJvIJUxv8wUlkB_BkffPq65k=/s.glbimg.com/in/fo/2014/10/24/10/23/29/4cd49797-9340-401b-9308-569074318e70.jpg','http://s2.glbimg.com/mMJp0Fl7zu603bAEFoo6DsIZsbg=/fit-in/718x0/s2.glbimg.com/QpqZONR2SA8m7VRhzNmw8C9DVc0=/640x0/s2.glbimg.com/NHXKJvIJUxv8wUlkB_BkffPq65k=/s.glbimg.com/in/fo/2014/10/24/10/23/29/4cd49797-9340-401b-9308-569074318e70.jpg',''),
	(5,'','http://s2.glbimg.com/f-4WzB7CjQHRD3GW-yGiodHzlY4=/640x0/s2.glbimg.com/mEZm0urjnY_yJY5WtaGQYwAOUXs=/s.glbimg.com/in/fo/2014/10/24/10/19/31/6cec9ab4-7946-47c9-bd3c-944b22f30118.jpg','','http://s2.glbimg.com/Ca9SObBT-NRrrHRDoGKOg7vKmFw=/fit-in/1080x1080/s2.glbimg.com/f-4WzB7CjQHRD3GW-yGiodHzlY4=/640x0/s2.glbimg.com/mEZm0urjnY_yJY5WtaGQYwAOUXs=/s.glbimg.com/in/fo/2014/10/24/10/19/31/6cec9ab4-7946-47c9-bd3c-944b22f30118.jpg','http://s2.glbimg.com/V--HCDMGhThQKHqsCrXj1QwjYQE=/fit-in/718x0/s2.glbimg.com/f-4WzB7CjQHRD3GW-yGiodHzlY4=/640x0/s2.glbimg.com/mEZm0urjnY_yJY5WtaGQYwAOUXs=/s.glbimg.com/in/fo/2014/10/24/10/19/31/6cec9ab4-7946-47c9-bd3c-944b22f30118.jpg',''),
	(6,'','http://s2.glbimg.com/k0QZIZ3DXrNP8Y7sN4EZhcLfMl8=/640x0/s2.glbimg.com/v5-p6Xn_iWYSljXn1iSamjRxUmI=/s.glbimg.com/in/fo/2014/10/24/10/40/13/ac104bf1-d7a2-49e6-9c07-83a6467a5684.jpg','','http://s2.glbimg.com/d3MwekLXgUgXrJXOjunYUUt29YQ=/fit-in/1080x1080/s2.glbimg.com/k0QZIZ3DXrNP8Y7sN4EZhcLfMl8=/640x0/s2.glbimg.com/v5-p6Xn_iWYSljXn1iSamjRxUmI=/s.glbimg.com/in/fo/2014/10/24/10/40/13/ac104bf1-d7a2-49e6-9c07-83a6467a5684.jpg','http://s2.glbimg.com/L5vY1fdEoIsNIKzCZmBqEh6BKy8=/fit-in/718x0/s2.glbimg.com/k0QZIZ3DXrNP8Y7sN4EZhcLfMl8=/640x0/s2.glbimg.com/v5-p6Xn_iWYSljXn1iSamjRxUmI=/s.glbimg.com/in/fo/2014/10/24/10/40/13/ac104bf1-d7a2-49e6-9c07-83a6467a5684.jpg',''),
	(7,'','http://s2.glbimg.com/Wa4xW4iqPrTK80lGV23QVAMfeUc=/640x0/s2.glbimg.com/Zo2DAx2-S5pjZfrzqWNlu8PGpRw=/s.glbimg.com/in/fo/2014/10/24/10/32/25/65e6bb11-dc11-40b5-9f55-76f16c23fac3.jpg','','http://s2.glbimg.com/5z6dr-30t1EdaV_AqiU7-ZgmdM8=/fit-in/1080x1080/s2.glbimg.com/Wa4xW4iqPrTK80lGV23QVAMfeUc=/640x0/s2.glbimg.com/Zo2DAx2-S5pjZfrzqWNlu8PGpRw=/s.glbimg.com/in/fo/2014/10/24/10/32/25/65e6bb11-dc11-40b5-9f55-76f16c23fac3.jpg','http://s2.glbimg.com/RRW-w7Yvp9qZ4B36jtO3mGAPd_A=/fit-in/718x0/s2.glbimg.com/Wa4xW4iqPrTK80lGV23QVAMfeUc=/640x0/s2.glbimg.com/Zo2DAx2-S5pjZfrzqWNlu8PGpRw=/s.glbimg.com/in/fo/2014/10/24/10/32/25/65e6bb11-dc11-40b5-9f55-76f16c23fac3.jpg',''),
	(8,'','http://s2.glbimg.com/f9QNUM3FBOSCHo_9O-nGMGo8mZ4=/640x0/s2.glbimg.com/UOv5UeDsFsdvNTQZjLR9Wrh717Y=/s.glbimg.com/in/fo/2014/10/24/10/26/40/cf35c3d8-5272-4298-ab88-9c845a9e945e.jpg','','http://s2.glbimg.com/HsqRIsiz5c54dYjBWxtx6G1Q8LU=/fit-in/1080x1080/s2.glbimg.com/f9QNUM3FBOSCHo_9O-nGMGo8mZ4=/640x0/s2.glbimg.com/UOv5UeDsFsdvNTQZjLR9Wrh717Y=/s.glbimg.com/in/fo/2014/10/24/10/26/40/cf35c3d8-5272-4298-ab88-9c845a9e945e.jpg','http://s2.glbimg.com/kbQC929DyCSn4fVz4_RwtMEZPbg=/fit-in/718x0/s2.glbimg.com/f9QNUM3FBOSCHo_9O-nGMGo8mZ4=/640x0/s2.glbimg.com/UOv5UeDsFsdvNTQZjLR9Wrh717Y=/s.glbimg.com/in/fo/2014/10/24/10/26/40/cf35c3d8-5272-4298-ab88-9c845a9e945e.jpg',''),
	(9,'','http://s2.glbimg.com/QpqZONR2SA8m7VRhzNmw8C9DVc0=/640x0/s2.glbimg.com/NHXKJvIJUxv8wUlkB_BkffPq65k=/s.glbimg.com/in/fo/2014/10/24/10/23/29/4cd49797-9340-401b-9308-569074318e70.jpg','','http://s2.glbimg.com/WPOiwz7jYuXgcoNSPfNeXXSd1Dk=/fit-in/1080x1080/s2.glbimg.com/QpqZONR2SA8m7VRhzNmw8C9DVc0=/640x0/s2.glbimg.com/NHXKJvIJUxv8wUlkB_BkffPq65k=/s.glbimg.com/in/fo/2014/10/24/10/23/29/4cd49797-9340-401b-9308-569074318e70.jpg','http://s2.glbimg.com/mMJp0Fl7zu603bAEFoo6DsIZsbg=/fit-in/718x0/s2.glbimg.com/QpqZONR2SA8m7VRhzNmw8C9DVc0=/640x0/s2.glbimg.com/NHXKJvIJUxv8wUlkB_BkffPq65k=/s.glbimg.com/in/fo/2014/10/24/10/23/29/4cd49797-9340-401b-9308-569074318e70.jpg',''),
	(10,'','http://s2.glbimg.com/f-4WzB7CjQHRD3GW-yGiodHzlY4=/640x0/s2.glbimg.com/mEZm0urjnY_yJY5WtaGQYwAOUXs=/s.glbimg.com/in/fo/2014/10/24/10/19/31/6cec9ab4-7946-47c9-bd3c-944b22f30118.jpg','','http://s2.glbimg.com/Ca9SObBT-NRrrHRDoGKOg7vKmFw=/fit-in/1080x1080/s2.glbimg.com/f-4WzB7CjQHRD3GW-yGiodHzlY4=/640x0/s2.glbimg.com/mEZm0urjnY_yJY5WtaGQYwAOUXs=/s.glbimg.com/in/fo/2014/10/24/10/19/31/6cec9ab4-7946-47c9-bd3c-944b22f30118.jpg','http://s2.glbimg.com/V--HCDMGhThQKHqsCrXj1QwjYQE=/fit-in/718x0/s2.glbimg.com/f-4WzB7CjQHRD3GW-yGiodHzlY4=/640x0/s2.glbimg.com/mEZm0urjnY_yJY5WtaGQYwAOUXs=/s.glbimg.com/in/fo/2014/10/24/10/19/31/6cec9ab4-7946-47c9-bd3c-944b22f30118.jpg',''),
	(11,'','http://s2.glbimg.com/k0QZIZ3DXrNP8Y7sN4EZhcLfMl8=/640x0/s2.glbimg.com/v5-p6Xn_iWYSljXn1iSamjRxUmI=/s.glbimg.com/in/fo/2014/10/24/10/40/13/ac104bf1-d7a2-49e6-9c07-83a6467a5684.jpg','','http://s2.glbimg.com/d3MwekLXgUgXrJXOjunYUUt29YQ=/fit-in/1080x1080/s2.glbimg.com/k0QZIZ3DXrNP8Y7sN4EZhcLfMl8=/640x0/s2.glbimg.com/v5-p6Xn_iWYSljXn1iSamjRxUmI=/s.glbimg.com/in/fo/2014/10/24/10/40/13/ac104bf1-d7a2-49e6-9c07-83a6467a5684.jpg','http://s2.glbimg.com/L5vY1fdEoIsNIKzCZmBqEh6BKy8=/fit-in/718x0/s2.glbimg.com/k0QZIZ3DXrNP8Y7sN4EZhcLfMl8=/640x0/s2.glbimg.com/v5-p6Xn_iWYSljXn1iSamjRxUmI=/s.glbimg.com/in/fo/2014/10/24/10/40/13/ac104bf1-d7a2-49e6-9c07-83a6467a5684.jpg',''),
	(12,'','http://s2.glbimg.com/Wa4xW4iqPrTK80lGV23QVAMfeUc=/640x0/s2.glbimg.com/Zo2DAx2-S5pjZfrzqWNlu8PGpRw=/s.glbimg.com/in/fo/2014/10/24/10/32/25/65e6bb11-dc11-40b5-9f55-76f16c23fac3.jpg','','http://s2.glbimg.com/5z6dr-30t1EdaV_AqiU7-ZgmdM8=/fit-in/1080x1080/s2.glbimg.com/Wa4xW4iqPrTK80lGV23QVAMfeUc=/640x0/s2.glbimg.com/Zo2DAx2-S5pjZfrzqWNlu8PGpRw=/s.glbimg.com/in/fo/2014/10/24/10/32/25/65e6bb11-dc11-40b5-9f55-76f16c23fac3.jpg','http://s2.glbimg.com/RRW-w7Yvp9qZ4B36jtO3mGAPd_A=/fit-in/718x0/s2.glbimg.com/Wa4xW4iqPrTK80lGV23QVAMfeUc=/640x0/s2.glbimg.com/Zo2DAx2-S5pjZfrzqWNlu8PGpRw=/s.glbimg.com/in/fo/2014/10/24/10/32/25/65e6bb11-dc11-40b5-9f55-76f16c23fac3.jpg',''),
	(13,'','http://s2.glbimg.com/f9QNUM3FBOSCHo_9O-nGMGo8mZ4=/640x0/s2.glbimg.com/UOv5UeDsFsdvNTQZjLR9Wrh717Y=/s.glbimg.com/in/fo/2014/10/24/10/26/40/cf35c3d8-5272-4298-ab88-9c845a9e945e.jpg','','http://s2.glbimg.com/HsqRIsiz5c54dYjBWxtx6G1Q8LU=/fit-in/1080x1080/s2.glbimg.com/f9QNUM3FBOSCHo_9O-nGMGo8mZ4=/640x0/s2.glbimg.com/UOv5UeDsFsdvNTQZjLR9Wrh717Y=/s.glbimg.com/in/fo/2014/10/24/10/26/40/cf35c3d8-5272-4298-ab88-9c845a9e945e.jpg','http://s2.glbimg.com/kbQC929DyCSn4fVz4_RwtMEZPbg=/fit-in/718x0/s2.glbimg.com/f9QNUM3FBOSCHo_9O-nGMGo8mZ4=/640x0/s2.glbimg.com/UOv5UeDsFsdvNTQZjLR9Wrh717Y=/s.glbimg.com/in/fo/2014/10/24/10/26/40/cf35c3d8-5272-4298-ab88-9c845a9e945e.jpg',''),
	(14,'','http://s2.glbimg.com/QpqZONR2SA8m7VRhzNmw8C9DVc0=/640x0/s2.glbimg.com/NHXKJvIJUxv8wUlkB_BkffPq65k=/s.glbimg.com/in/fo/2014/10/24/10/23/29/4cd49797-9340-401b-9308-569074318e70.jpg','','http://s2.glbimg.com/WPOiwz7jYuXgcoNSPfNeXXSd1Dk=/fit-in/1080x1080/s2.glbimg.com/QpqZONR2SA8m7VRhzNmw8C9DVc0=/640x0/s2.glbimg.com/NHXKJvIJUxv8wUlkB_BkffPq65k=/s.glbimg.com/in/fo/2014/10/24/10/23/29/4cd49797-9340-401b-9308-569074318e70.jpg','http://s2.glbimg.com/mMJp0Fl7zu603bAEFoo6DsIZsbg=/fit-in/718x0/s2.glbimg.com/QpqZONR2SA8m7VRhzNmw8C9DVc0=/640x0/s2.glbimg.com/NHXKJvIJUxv8wUlkB_BkffPq65k=/s.glbimg.com/in/fo/2014/10/24/10/23/29/4cd49797-9340-401b-9308-569074318e70.jpg',''),
	(15,'','http://s2.glbimg.com/f-4WzB7CjQHRD3GW-yGiodHzlY4=/640x0/s2.glbimg.com/mEZm0urjnY_yJY5WtaGQYwAOUXs=/s.glbimg.com/in/fo/2014/10/24/10/19/31/6cec9ab4-7946-47c9-bd3c-944b22f30118.jpg','','http://s2.glbimg.com/Ca9SObBT-NRrrHRDoGKOg7vKmFw=/fit-in/1080x1080/s2.glbimg.com/f-4WzB7CjQHRD3GW-yGiodHzlY4=/640x0/s2.glbimg.com/mEZm0urjnY_yJY5WtaGQYwAOUXs=/s.glbimg.com/in/fo/2014/10/24/10/19/31/6cec9ab4-7946-47c9-bd3c-944b22f30118.jpg','http://s2.glbimg.com/V--HCDMGhThQKHqsCrXj1QwjYQE=/fit-in/718x0/s2.glbimg.com/f-4WzB7CjQHRD3GW-yGiodHzlY4=/640x0/s2.glbimg.com/mEZm0urjnY_yJY5WtaGQYwAOUXs=/s.glbimg.com/in/fo/2014/10/24/10/19/31/6cec9ab4-7946-47c9-bd3c-944b22f30118.jpg','');

/*!40000 ALTER TABLE `feed_image` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table feed_post
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feed_post`;

CREATE TABLE `feed_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `body` varchar(600) NOT NULL,
  `status` varchar(11) NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `video_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `feed_post_4f331e2f` (`author_id`),
  KEY `feed_post_f33175e6` (`image_id`),
  KEY `feed_post_b58b747e` (`video_id`),
  CONSTRAINT `feed_post_video_id_3b6a1ae4c61477c2_fk_feed_video_id` FOREIGN KEY (`video_id`) REFERENCES `feed_video` (`id`),
  CONSTRAINT `feed_post_author_id_369a4d566789feab_fk_feed_author_id` FOREIGN KEY (`author_id`) REFERENCES `feed_author` (`id`),
  CONSTRAINT `feed_post_image_id_85f38d41725ae00_fk_feed_image_id` FOREIGN KEY (`image_id`) REFERENCES `feed_image` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

LOCK TABLES `feed_post` WRITE;
/*!40000 ALTER TABLE `feed_post` DISABLE KEYS */;

INSERT INTO `feed_post` (`id`, `created_at`, `body`, `status`, `author_id`, `image_id`, `video_id`)
VALUES
	(1,'2014-11-13 01:18:51','Qualquer lesão ou bolinha que aparece com relevo na pele, geralmente, já é chamada de verruga, mas nem sempre é. As verrugas verdadeiras são virais, provocadas por um tipo do vírus HPV, e são mais comuns em quem tem a imunidade baixa. Você pode rever todas as informações do programa desta sexta-feira e tentar identificar se o que você tem é verruga ou não no nosso site: <a href=\"http://glo.bo/1oBq1p0\" target=\"_blank\">http://glo.bo/1oBq1p0</a>','P',1,NULL,NULL),
	(2,'2014-11-13 01:18:52','Você sabia que verruga pega? Pois é, ela é contagiosa! Por isso é importante diagnosticar a doença, fazer o tratamento adequado e nunca cutucar. Veja como ocorre o contágio no vídeo abaixo: ','P',2,NULL,3717841),
	(3,'2014-11-13 01:18:52','Só um dermatologista pode fazer o diagnóstico de uma verruga e definir o tratamento que, em geral, é feito com o uso de ácido. O tratamento da verruga é longo, e é importante que o paciente não desista no meio do caminho! ','P',3,NULL,3717839),
	(4,'2014-11-13 01:18:52','A verdadeira verruga é sempre causada pelo vírus e, normalmente, ela é uma \"bolinha\" sobressalente com a superfície rugosa. Veja mais detalhes sobre as características da verruga no vídeo:','P',4,NULL,3717838),
	(5,'2014-11-13 01:18:52','Tenho essas pintas (verrugas) no pescoço, nas costas e na barriga... Tem algum tratamento?','P',5,1,NULL),
	(6,'2014-11-13 01:18:52','Meu filho tem essas verrugas. Como posso tirar sem fazer cirurgia?','P',6,2,NULL),
	(7,'2014-11-13 01:18:52','É muito comum as crianças terem verruga, porque além de terem a imunidade ainda em formação, elas costumam mexer, cutucar. Uma recomendação importante é para que a pessoa nunca tente arrancar a saliência. E se você não sabe se o que você tem é verruga, pinta ou qualquer outra coisa, na dúvida, procure um dermatologista. ','P',7,NULL,NULL),
	(8,'2014-11-13 01:18:52','Isso pode ser uma verruga? Às vezes incomoda.','P',8,3,NULL),
	(9,'2014-11-13 01:18:52','Bom dia, estou com isso no pé, será que é verruga? Estou em dúvida.','P',9,4,NULL),
	(10,'2014-11-13 01:18:52','Minha amiga tem essas nos dedos, que não saram nunca. O que ela deve fazer? Já passou vários produtos e foi ao dermatologista, mas não adiantou.','P',10,5,NULL),
	(11,'2014-11-13 01:24:32','Qualquer lesão ou bolinha que aparece com relevo na pele, geralmente, já é chamada de verruga, mas nem sempre é. As verrugas verdadeiras são virais, provocadas por um tipo do vírus HPV, e são mais comuns em quem tem a imunidade baixa. Você pode rever todas as informações do programa desta sexta-feira e tentar identificar se o que você tem é verruga ou não no nosso site: <a href=\"http://glo.bo/1oBq1p0\" target=\"_blank\">http://glo.bo/1oBq1p0</a>','P',11,NULL,NULL),
	(12,'2014-11-13 01:24:32','Você sabia que verruga pega? Pois é, ela é contagiosa! Por isso é importante diagnosticar a doença, fazer o tratamento adequado e nunca cutucar. Veja como ocorre o contágio no vídeo abaixo: ','P',12,NULL,3717841),
	(13,'2014-11-13 01:24:33','Só um dermatologista pode fazer o diagnóstico de uma verruga e definir o tratamento que, em geral, é feito com o uso de ácido. O tratamento da verruga é longo, e é importante que o paciente não desista no meio do caminho! ','P',13,NULL,3717839),
	(14,'2014-11-13 01:24:33','A verdadeira verruga é sempre causada pelo vírus e, normalmente, ela é uma \"bolinha\" sobressalente com a superfície rugosa. Veja mais detalhes sobre as características da verruga no vídeo:','P',14,NULL,3717838),
	(15,'2014-11-13 01:24:33','Tenho essas pintas (verrugas) no pescoço, nas costas e na barriga... Tem algum tratamento?','P',15,6,NULL),
	(16,'2014-11-13 01:24:33','Meu filho tem essas verrugas. Como posso tirar sem fazer cirurgia?','P',16,7,NULL),
	(17,'2014-11-13 01:24:33','É muito comum as crianças terem verruga, porque além de terem a imunidade ainda em formação, elas costumam mexer, cutucar. Uma recomendação importante é para que a pessoa nunca tente arrancar a saliência. E se você não sabe se o que você tem é verruga, pinta ou qualquer outra coisa, na dúvida, procure um dermatologista. ','P',17,NULL,NULL),
	(18,'2014-11-13 01:24:33','Isso pode ser uma verruga? Às vezes incomoda.','P',18,8,NULL),
	(19,'2014-11-13 01:24:33','Bom dia, estou com isso no pé, será que é verruga? Estou em dúvida.','P',19,9,NULL),
	(20,'2014-11-13 01:24:33','Minha amiga tem essas nos dedos, que não saram nunca. O que ela deve fazer? Já passou vários produtos e foi ao dermatologista, mas não adiantou.','P',20,10,NULL),
	(21,'2014-11-13 01:26:04','Qualquer lesão ou bolinha que aparece com relevo na pele, geralmente, já é chamada de verruga, mas nem sempre é. As verrugas verdadeiras são virais, provocadas por um tipo do vírus HPV, e são mais comuns em quem tem a imunidade baixa. Você pode rever todas as informações do programa desta sexta-feira e tentar identificar se o que você tem é verruga ou não no nosso site: <a href=\"http://glo.bo/1oBq1p0\" target=\"_blank\">http://glo.bo/1oBq1p0</a>','P',21,NULL,NULL),
	(22,'2014-11-13 01:26:04','Você sabia que verruga pega? Pois é, ela é contagiosa! Por isso é importante diagnosticar a doença, fazer o tratamento adequado e nunca cutucar. Veja como ocorre o contágio no vídeo abaixo: ','P',22,NULL,3717841),
	(23,'2014-11-13 01:26:04','Só um dermatologista pode fazer o diagnóstico de uma verruga e definir o tratamento que, em geral, é feito com o uso de ácido. O tratamento da verruga é longo, e é importante que o paciente não desista no meio do caminho! ','P',23,NULL,3717839),
	(24,'2014-11-13 01:26:04','A verdadeira verruga é sempre causada pelo vírus e, normalmente, ela é uma \"bolinha\" sobressalente com a superfície rugosa. Veja mais detalhes sobre as características da verruga no vídeo:','P',24,NULL,3717838),
	(25,'2014-11-13 01:26:04','Tenho essas pintas (verrugas) no pescoço, nas costas e na barriga... Tem algum tratamento?','P',25,11,NULL),
	(26,'2014-11-13 01:26:04','Meu filho tem essas verrugas. Como posso tirar sem fazer cirurgia?','P',26,12,NULL),
	(27,'2014-11-13 01:26:04','É muito comum as crianças terem verruga, porque além de terem a imunidade ainda em formação, elas costumam mexer, cutucar. Uma recomendação importante é para que a pessoa nunca tente arrancar a saliência. E se você não sabe se o que você tem é verruga, pinta ou qualquer outra coisa, na dúvida, procure um dermatologista. ','P',27,NULL,NULL),
	(28,'2014-11-13 01:26:04','Isso pode ser uma verruga? Às vezes incomoda.','P',28,13,NULL),
	(29,'2014-11-13 01:26:04','Bom dia, estou com isso no pé, será que é verruga? Estou em dúvida.','P',29,14,NULL),
	(30,'2014-11-13 01:26:04','Minha amiga tem essas nos dedos, que não saram nunca. O que ela deve fazer? Já passou vários produtos e foi ao dermatologista, mas não adiantou.','P',30,15,NULL);

/*!40000 ALTER TABLE `feed_post` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table feed_video
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feed_video`;

CREATE TABLE `feed_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thumb` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3717842 DEFAULT CHARSET=utf8;

LOCK TABLES `feed_video` WRITE;
/*!40000 ALTER TABLE `feed_video` DISABLE KEYS */;

INSERT INTO `feed_video` (`id`, `thumb`)
VALUES
	(3717838,'http://s01.video.glbimg.com/640x360/3717838.jpg'),
	(3717839,'http://s01.video.glbimg.com/640x360/3717839.jpg'),
	(3717841,'http://s01.video.glbimg.com/640x360/3737961.jpg');

/*!40000 ALTER TABLE `feed_video` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
