# -*- coding: utf-8 -*-

from django.db import models
from django.utils import timezone


class Author(models.Model):
    class Meta:
        verbose_name = "Author"
        verbose_name_plural = "Authors"

    role = models.BooleanField(default=False)
    avatar = models.URLField(blank=True, null=True)
    name = models.CharField(max_length=40, default='Desconhecido')

    def __unicode__(self):
        return self.name


class Video(models.Model):
    class Meta:
        verbose_name = "Video"
        verbose_name_plural = "Videos"

    id = models.AutoField(primary_key=True, max_length=30)
    thumb = models.URLField()

    def __unicode__(self):
        return self.id


class Image(models.Model):
    class Meta:
        verbose_name = "Image"
        verbose_name_plural = "Images"

    url = models.URLField()
    croppedUrl = models.URLField(max_length=300, blank=True, null=True)
    credits = models.CharField(max_length=50, blank=True, null=True)
    imageLightUrl = models.URLField(max_length=300, blank=True, null=True)
    imageFeedUrl = models.URLField(max_length=300, blank=True, null=True)
    subject = models.CharField(max_length=50, blank=True, null=True)

    def __unicode__(self):
        return self.url


class Post(models.Model):
    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Posts"

    STATUS_OPTIONS = (
        (u'U', u'Unpublished'),
        (u'P', u'Published'),
        (u'S', u'Scheduled'),
        (u'D', u'Draft'),
    )

    id = models.AutoField(primary_key=True, max_length=30)
    author = models.ForeignKey(Author, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    body = models.CharField(max_length=600)
    status = models.CharField(max_length=11,
                              choices=STATUS_OPTIONS,
                              default='P')
    video = models.ForeignKey(Video, default=None, blank=True, null=True)
    image = models.ForeignKey(Image, default=None, blank=True, null=True)

    @property
    def created_at_in_words(self):
        HOURS_IN_SECONDS = 3600
        MINUTES_IN_SECONDS = 60

        hours = ''
        minutes = ''
        message = 'Há '
        now = timezone.now()
        diff = now - self.created_at
        seconds = diff.seconds

        diff_divided_by_hours = seconds / HOURS_IN_SECONDS
        diff_divided_by_minutes = seconds / MINUTES_IN_SECONDS
        if diff_divided_by_hours > 1:
            hours = '%s horas' % int(diff_divided_by_hours)
            seconds -= diff_divided_by_hours * HOURS_IN_SECONDS
            diff_divided_by_minutes = seconds / MINUTES_IN_SECONDS
            message += hours
            if diff_divided_by_minutes > 1:
                message += ' e %s minutos' % int(diff_divided_by_minutes)
            return message + '.'

        elif diff_divided_by_minutes > 1:
            minutes = '%s minutos' % int(diff_divided_by_minutes)
            return (message + minutes + '.')
        else:
            return (message + str(int(seconds)) + ' segundos.')

    def __unicode__(self):
        return 'Author: %s - body: %s' % (self.author, self.body)
